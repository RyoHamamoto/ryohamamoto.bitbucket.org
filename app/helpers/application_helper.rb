module ApplicationHelper

  BASE_TITLE = "Ruby on Rails Tutorial Sample App"
  
  # ページごとの完全なタイトルを返します。                   # コメント行
  def full_title(page_title = '')                     # メソッド定義とオプション引数
    if page_title.empty?                              # 論理値テスト
      BASE_TITLE                                      # 暗黙の戻り値
    else 
      page_title + " | " + BASE_TITLE                 # 文字列の結合
    end
  end
end